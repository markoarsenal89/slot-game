import { combineReducers } from 'redux'
import { spinStarted, bet } from './components/Controls/reducers'
import { winLineLength } from './components/Reels/reducers'

const reducers = combineReducers({
  spinStarted,
  bet,
  winLineLength,
})

export default reducers

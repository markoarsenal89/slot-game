import april from './assets/images/april.png'
import bebop from './assets/images/bebop.png'
import casey from './assets/images/casey.png'
import donatello from './assets/images/donatello.png'
import krang from './assets/images/krang.png'
import leonardo from './assets/images/leonardo.png'
import michaelangelo from './assets/images/michaelangelo.png'
import pizza from './assets/images/pizza.png'
import raphael from './assets/images/raphael.png'
import rocksteady from './assets/images/rocksteady.png'
import shredder from './assets/images/shredder.png'
import splinter from './assets/images/splinter.png'

export const items = [
  {
    value: 0,
    img: april,
  },
  {
    value: 1,
    img: bebop,
  },
  {
    value: 2,
    img: casey,
  },
  {
    value: 3,
    img: donatello,
  },
  {
    value: 4,
    img: krang,
  },
  {
    value: 5,
    img: leonardo,
  },
  {
    value: 6,
    img: michaelangelo,
  },
  {
    value: 7,
    img: pizza,
  },
  {
    value: 8,
    img: raphael,
  },
  {
    value: 9,
    img: rocksteady,
  },
  {
    value: 10,
    img: shredder,
  },
  {
    value: 11,
    img: splinter,
  },
]

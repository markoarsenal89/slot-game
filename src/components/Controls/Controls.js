import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { ControlsContainerStyled, CircleBtnStyled, BtnStyled } from './controls.styles'
import { startSpin, setBet, setBetMax } from './actions'

const Controls = () => {
  const dispatch = useDispatch()
  const spinStarted = useSelector(state => state.spinStarted)

  const startSpinning = () => {
    if (!spinStarted) {
      dispatch(startSpin)
    }
  }
  const setBetHandler = () => {
    dispatch(setBet)
  }
  const setBetMaxHandler = () => {
    dispatch(setBetMax)
  }

  return (
    <ControlsContainerStyled>
      <BtnStyled type="button" onClick={setBetHandler} disabled={spinStarted ? true : false}>
        Bet One
      </BtnStyled>
      <CircleBtnStyled type="button" onClick={startSpinning} disabled={spinStarted ? true : false}>
        Spin
      </CircleBtnStyled>
      <BtnStyled type="button" onClick={setBetMaxHandler} disabled={spinStarted ? true : false}>
        Bet Max
      </BtnStyled>
    </ControlsContainerStyled>
  )
}

export default Controls

import styled, { css } from 'styled-components'

export const ControlsContainerStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px;
  background-color: #44596c;
`

const disabledStyles = css`
  cursor: default;
  opacity: 0.5;
  box-shadow: none;
`

export const CircleBtnStyled = styled.button`
  width: 80px;
  height: 80px;
  margin: 0 10px;
  border: 4px solid #92be67;
  border-radius: 50%;
  font-weight: bold;
  color: #92be67;
  cursor: pointer;
  background-color: #eee;
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.5);
  text-transform: uppercase;
  ${({ disabled }) => (disabled ? disabledStyles : '')}
  &:active {
    box-shadow: none;
  }

  @media (min-width: 768px) {
    margin: 0 20px;
  }
`

export const BtnStyled = styled.div`
  padding: 10px 20px;
  border-radius: 3px;
  color: #fff;
  background-color: #01aee6;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
  cursor: pointer;
  &:active {
    box-shadow: none;
  }

  @media (min-width: 768px) {
    padding: 12px 30px;
  }
`

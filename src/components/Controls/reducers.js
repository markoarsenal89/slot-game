import { START_SPIN, STOP_SPIN, SET_BET, SET_BET_MAX, RESET_BET } from './actions'

export const spinStarted = (state = false, action) => {
  switch (action.type) {
    case START_SPIN:
      return true
    case STOP_SPIN:
      return false
    default:
      return state
  }
}

export const bet = (state = 1, action) => {
  switch (action.type) {
    case SET_BET:
      return state < 20 ? state + 1 : state
    case SET_BET_MAX:
      return 20
    case RESET_BET:
      return 1
    default:
      return state
  }
}

export const START_SPIN = 'START_SPIN'
export const STOP_SPIN = 'STOP_SPIN'
export const SET_BET = 'SET_BET'
export const SET_BET_MAX = 'SET_BET_MAX'
export const RESET_BET = 'RESET_BET'

export const startSpin = { type: START_SPIN }
export const stopSpin = { type: STOP_SPIN }
export const setBet = { type: SET_BET }
export const setBetMax = { type: SET_BET_MAX }
export const resetBet = { type: RESET_BET }

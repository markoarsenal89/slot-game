export const WIN_LINE_LENGTH = 'WIN_LINE_LENGTH'

export const setWinLineLength = length => ({ type: WIN_LINE_LENGTH, length })

import styled from 'styled-components'
import newyork from '../../assets/images/newyork.jpg'

export const WrapperStyled = styled.div`
  height: calc(((100vw - 2 * 20px - 4 * 2px) / 5) * 3 + 2 * 2px + 2 * 20px);
  padding: 20px;
  background-image: url(${newyork});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;

  @media (min-width: 768px) {
    height: calc(((100vw - 2 * 40px - 4 * 4px) / 5) * 3 + 2 * 4px + 2 * 40px);
    padding: 40px;
  }

  @media (min-width: 1024px) {
    height: calc(((1024px - 2 * 40px - 4 * 4px) / 5) * 3 + 2 * 4px + 2 * 40px);
  }
`

export const ReelsContainerStyled = styled.div`
  display: grid;
  grid-template-columns: repeat(5, 1fr);
  grid-column-gap: 2px;
  width: 100%;
  height: 100%;
  overflow: hidden;

  @media (min-width: 768px) {
    grid-column-gap: 4px;
  }
`

export const ReelTrackStyled = styled.div`
  flex: 0 0 20%;
  position: relative;
`

export const ReelTrackContainerStyled = styled.div`
  position: absolute;
  left: 0;
  bottom: 0;
  width: 100%;
`

export const ReelItemStyled = styled.div`
  padding-top: 100%;
  background-position: center;
  background-size: cover;
  position: relative;
  background-color: rgba(255, 255, 255, 0.9);
  & + & {
    margin-top: 2px;
  }
  &.active {
    background-color: #fff;
    &::after {
      content: '';
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      border: 2px solid #ff9393;
    }
  }

  @media (min-width: 768px) {
    & + & {
      margin-top: 4px;
    }
    &.active::after {
      border-width: 4px;
    }
  }
`

import { WIN_LINE_LENGTH } from './actions'

export function winLineLength(state = 0, action) {
  switch (action.type) {
    case WIN_LINE_LENGTH:
      return action.length
    default:
      return state
  }
}

import React, { useRef, useEffect, createRef, memo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import uuid from 'uuid/v4'
import { shuffle } from '../../utils/index'
import {
  WrapperStyled,
  ReelsContainerStyled,
  ReelTrackStyled,
  ReelTrackContainerStyled,
  ReelItemStyled,
} from './reels.styles'
import { items } from '../../config'
import { stopSpin } from '../Controls/actions'
import { setWinLineLength } from './actions'

const generateReels = items => {
  const newItems = [...Array(5)].map((el, i) => {
    const trackArr = {
      id: uuid(),
      elements: [],
    }

    for (let j = 0; j < 4; j++) {
      trackArr.elements.push(
        ...shuffle(
          items.map(item => {
            return {
              id: `${uuid()}-${i}-${j}`,
              ...item,
            }
          }),
        ),
      )
    }

    return trackArr
  })

  return newItems
}

let reels = generateReels(items)
const finalCombination = []
let boxH = 0

const animateReel = ({ reelEl, boxHeight, duration, onAnimationEnd }) => {
  let iterator = 1
  let start = null
  let animationProgress = 0
  const animationStep = boxHeight / 4
  const numberOfIterations = Math.floor(duration / boxHeight) * 4

  function step(timestamp) {
    if (!start) {
      start = timestamp
    }

    animationProgress += animationStep

    if (reelEl && reelEl.style) {
      reelEl.style.transform = `translateY(${animationProgress}px)`
    }

    if (iterator < numberOfIterations) {
      window.requestAnimationFrame(step)
    } else {
      const firstItem = numberOfIterations / 4
      const choosenItems = [firstItem, firstItem + 1, firstItem + 2]

      onAnimationEnd(choosenItems)
    }

    iterator++
  }

  window.requestAnimationFrame(step)
}

const animateAllReels = (reelTracks, animationEndCallback, dispatch) => {
  dispatch(setWinLineLength(0))
  ;[...Array(5)].forEach((el, i) => {
    animateReel({
      reelEl: reelTracks.current[i].current,
      boxHeight: boxH,
      duration: 2000 + i * 200,
      onAnimationEnd: items => {
        animationEndCallback(items, i, reelTracks, dispatch)
      },
    })
  })
}

const animationEndCallback = (choosenItems, i, reelTracks, dispatch) => {
  finalCombination.push(choosenItems)

  if (i === 4) {
    const winCombination = calculateWin(finalCombination, reels)

    showWinCombination(reelTracks, winCombination.row.columns)
    dispatch(stopSpin)
    dispatch(setWinLineLength(winCombination.row.value))
  }
}

const calculateWin = (combination, allItems) => {
  const matrix = createFinalMatrix(combination, allItems)
  const rows = []

  for (let i = 0; i < matrix[0].length; i++) {
    const row = []

    for (let j = 0; j < matrix.length; j++) {
      row.push(matrix[j][i])
    }

    const rowCombination = checkRowCombination(row)
    rows.push(rowCombination)
  }

  const winCombination = checkBestRow(rows)
  return winCombination
}

const showWinCombination = (tracks, winColumns) => {
  for (let i = 0; i < winColumns.length; i++) {
    const column = winColumns[i]
    const track = tracks.current[column.index].current
    const elIndex = column.element.index
    const trackEl = track.childNodes[elIndex]

    trackEl.classList.add('active')
  }
}

const checkBestRow = rows => {
  const best = {
    index: 0,
    row: rows[0],
  }

  for (let i = 0; i < rows.length; i++) {
    if (rows[i + 1] && rows[i].value < rows[i + 1].value) {
      best.index = i + 1
      best.row = rows[i + 1]
    }
  }

  return best
}

const checkRowCombination = row => {
  const longestHit = {
    columns: [],
    value: 0,
  }
  let counter = 1

  for (let i = 0; i < row.length; i++) {
    if (row[i + 1] && row[i].element && row[i + 1].element && row[i].element.value === row[i + 1].element.value) {
      const element = {
        index: i,
        element: row[i],
      }

      if (!longestHit.columns.includes(element)) {
        longestHit.columns.push(element)
      }

      longestHit.columns.push({
        index: i + 1,
        element: row[i + 1],
      })
      counter++
    }
  }

  longestHit.value = counter

  return longestHit
}

const createFinalMatrix = (combination, allItems) => {
  const matrix = []

  for (let i = 0; i < allItems.length; i++) {
    const matrixColumn = []

    for (let j = combination[0].length - 1; j >= 0; j--) {
      const index = allItems[i].elements.length - 1 - combination[i][j]

      matrixColumn.push({
        index,
        element: allItems[i].elements[index],
      })
    }

    matrix.push(matrixColumn)
  }

  return matrix
}

const Reels = () => {
  const dispatch = useDispatch()
  const spinStarted = useSelector(state => state.spinStarted)
  const reelTracks = useRef([...Array(5)].map(() => createRef()))

  if (spinStarted) {
    reels = generateReels(items)
  }

  // Calculate box dimensions
  useEffect(() => {
    const el = reelTracks.current[0].current.childNodes[1]
    const style = el.currentStyle || window.getComputedStyle(el)
    const elPaddingTop = parseFloat(style.paddingTop.replace('px', ''))
    const elMarginTop = parseFloat(style.marginTop.replace('px', ''))

    boxH = elPaddingTop + elMarginTop
  }, [])

  useEffect(() => {
    if (spinStarted) {
      animateAllReels(reelTracks, animationEndCallback, dispatch)
    }
  }, [spinStarted])

  return (
    <WrapperStyled>
      <ReelsContainerStyled>
        {reels.map((reel, i) => {
          return (
            <ReelTrackStyled key={reel.id}>
              <ReelTrackContainerStyled ref={reelTracks.current[i]}>
                {reel.elements.map(item => {
                  return <ReelItemStyled key={item.id} style={{ backgroundImage: `url(${item.img})` }} />
                })}
              </ReelTrackContainerStyled>
            </ReelTrackStyled>
          )
        })}
      </ReelsContainerStyled>
    </WrapperStyled>
  )
}

export default Reels

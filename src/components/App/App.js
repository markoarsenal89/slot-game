import React from 'react'
import Reels from '../Reels/Reels'
import Stats from '../Stats/Stats'
import Controls from '../Controls/Controls'
import { ResetStyles, GlobalStyles } from './app.styles'

function App() {
  return (
    <>
      <ResetStyles />
      <GlobalStyles />
      <>
        <Reels />
        <Stats />
        <Controls />
      </>
    </>
  )
}

export default App

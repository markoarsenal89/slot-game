import React, { useState, useEffect } from 'react'
import { WrapperStyled, StatsItemStyled, StatsItemValueStyled } from './stats.styles'
import { useSelector, useDispatch } from 'react-redux'
import { resetBet } from '../Controls/actions'

const newStats = (oldStats, winLine) => {
  const win = winLine > 1 ? oldStats.bet * Math.pow(winLine, 2) : 0
  const coins = oldStats.coins + win - oldStats.bet

  return {
    coins,
    bet: 1,
    win,
  }
}

const Stats = () => {
  const dispatch = useDispatch()
  const bet = useSelector(state => state.bet)
  const winLine = useSelector(state => state.winLineLength)
  const spinStarted = useSelector(state => state.spinStarted)
  const [stats, setStats] = useState({
    coins: 12000,
    bet,
    win: 0,
  })

  useEffect(() => {
    setStats(oldStats => {
      return { ...oldStats, coins: oldStats.coins - bet, bet }
    })
  }, [bet])

  useEffect(() => {
    if (winLine > 1) {
      setStats(oldStats => {
        return newStats(oldStats, winLine)
      })
    }
  }, [winLine])

  useEffect(() => {
    if (spinStarted) {
      setStats(oldStats => {
        return { ...oldStats, win: 0 }
      })
    } else {
      setTimeout(() => {
        dispatch(resetBet)
      })
    }
  }, [spinStarted])

  return (
    <WrapperStyled>
      <StatsItemStyled>
        Coins: <StatsItemValueStyled>{stats.coins}</StatsItemValueStyled>
      </StatsItemStyled>
      <StatsItemStyled>
        Bet: <StatsItemValueStyled>{stats.bet}</StatsItemValueStyled>
      </StatsItemStyled>
      <StatsItemStyled>
        Win: <StatsItemValueStyled>{stats.win}</StatsItemValueStyled>
      </StatsItemStyled>
    </WrapperStyled>
  )
}

export default Stats

import styled from 'styled-components'

export const WrapperStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 8px;
  margin-top: auto;
  font-size: 14px;
  font-weight: bold;
  color: #fff;
  background-color: #bfafaf;
`

export const StatsItemStyled = styled.div`
  margin: 0 20px;
`

export const StatsItemValueStyled = styled.span`
  font-weight: normal;
`
